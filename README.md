# ci-test

GitLab CI test suite by TPA. Those are jobs ran (possibly regularly)
to make sure CI is working.

Those are the jobs that are run on push, but they are also ran on a
daily schedule, [configured in the project](https://gitlab.torproject.org/tpo/tpa/ci-test/-/pipeline_schedules/15/edit) at 2AM UTC.
